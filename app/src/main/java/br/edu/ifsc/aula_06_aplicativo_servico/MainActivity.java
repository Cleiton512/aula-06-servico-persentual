package br.edu.ifsc.aula_06_aplicativo_servico;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private TextView txt_percent;
    private TextView tipTextView;
    private TextView totalTextView;
    private EditText edt_num;
    SeekBar percentSeekBar;


    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat = NumberFormat.getPercentInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_percent = findViewById(R.id.txt_percent);
        tipTextView = findViewById(R.id.tipTextView);
        totalTextView = findViewById(R.id.totalTextView);

        tipTextView.setText(currencyFormat.format(0));
        totalTextView.setText(currencyFormat.format(0));

        edt_num = findViewById(R.id.edt_num);

        percentSeekBar = findViewById(R.id.percentSeekBar);

        percentSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                txt_percent.setText(percentFormat.format(seekBar.getProgress()/100.0));
                calcular();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void calcular(){
        //PEGA VALOR DO VARIAVEL EDT NUMERO
        double valor = Double.parseDouble(edt_num.getText().toString());
        double tip =  valor * percentSeekBar.getProgress()/100.0;
        double total = valor + tip;

        tipTextView.setText(currencyFormat.format(tip));
        totalTextView.setText(currencyFormat.format((total)));
    }

}
